$(function() {

    //Menu
    $('.menu__btn, .menu__link').on('click', function(){
        $('.menu__list').toggleClass('menu__list--active');
    });

	// Smooth Scroll
    $('.menu, .surfer').on('click','a', function (event) {
		event.preventDefault();

		var id  = $(this).attr('href'),
			top = $(id).offset().top;

		$('body,html').animate({scrollTop: top}, 1500);
	});

    // Fixed Header
    var header = $('.header'),
        surferHeight = $('.surfer').innerHeight(),
        scrollOffset = $(window).scrollTop();

        checkScroll(scrollOffset);

    $(window).on("scroll", function() {

        scrollOffset=$(this).scrollTop();
        checkScroll(scrollOffset);

    });

    function checkScroll(scrollOffset) {

        if( scrollOffset >= surferHeight) {
        header.addClass('header--fixed');
        } else {
        header.removeClass('header--fixed');
        }
    }

    // Slick Slider
    $('.blog__items').slick({
        autoplay: true,
		arrows: false,
		dots: true,
  		infinite: true,
    });
    
    // MixItUp
    const mixer = mixitup('.galery__content');

});